#include <stdio.h>
int p(int);
int r(int);
int c(int);
int at(int);

int p(int tp)
{
	return r(tp)-c(tp);
}

int r(int tp)
{
	return at(tp)*tp;
}

int c(int tp)
{
	return 500+(3*at(tp));
}

int at(int tp)
{
	return 120+(20*(15-tp)/5);
}

int main ()
{
	int tp;
	printf("Ticket price\t No. of attendees\t profit \n");
	for (tp=0;tp<50;tp=tp+5)
	{
		printf("Rs.%d\t\t %d\t\t\t %d\n",tp,at(tp),p(tp));
	}
	
	return 0;
}
